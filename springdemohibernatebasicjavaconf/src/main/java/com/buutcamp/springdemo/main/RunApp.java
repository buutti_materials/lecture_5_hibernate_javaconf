package com.buutcamp.springdemo.main;

import com.buutcamp.springdemo.SpringConf;
import com.buutcamp.springdemo.entities.BuutcampEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

public class RunApp {

    public RunApp() {
        ApplicationContext appCtx = new AnnotationConfigApplicationContext(SpringConf.class);
        Environment env = appCtx.getEnvironment();

        Configuration conf = new Configuration();
        conf.setProperty("hibernate.connection.driver_class", env.getProperty("hibernate.driver"));
        conf.setProperty("hibernate.connection.url", env.getProperty("hibernate.url"));
        conf.setProperty("hibernate.connection.username", env.getProperty("hibernate.username"));
        conf.setProperty("hibernate.connection.password", env.getProperty("hibernate.password"));
        conf.setProperty("hibernate.connection.pool_size", "1");
        conf.setProperty("hibernate.current_session_context_class","thread");
        conf.setProperty("hibernate.dialect","org.hibernate.dialect.MySQL8Dialect");
        conf.setProperty("hibernate.hbm2ddl.auto","update");

        //SessionFactory factory = new Configuration().configure().addAnnotatedClass(com.buutcamp.springdemo.entities.BuutcampEntity.class).buildSessionFactory();
        SessionFactory factory = conf.addAnnotatedClass(BuutcampEntity.class).buildSessionFactory();
        //SessionFactory factory = conf.addAnnotatedClass(com.buutcamp.springdemo.entities.BuutcampEntity.class).buildSessionFactory();
        Session session = factory.getCurrentSession();

        try {
            session.beginTransaction();
            session.save(new BuutcampEntity("asdqwetoimii?",11232,"a"));
            session.getTransaction().commit();
        } finally {
            session.close();
            factory.close();
        }
    }
}
