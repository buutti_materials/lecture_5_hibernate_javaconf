package com.buutcamp.springdemo;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:hibernate.properties")
public class SpringConf {
}
