package com.buutcamp.springdemo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//In a sense this is a _database_ object class
@Entity
@Table(name="buutcamp_table")
public class BuutcampEntity {

    @Id
    @Column(name="id")
    private int id;

    @Column(name="col_1")
    private String col_1;

    @Column(name="col_2")
    private int col_2;

    @Column(name="col_3")
    private String col_3;

    public BuutcampEntity() {}

    public BuutcampEntity(String col_1, int col_2, String col_3) {
        this.col_1 = col_1;
        this.col_2 = col_2;
        this.col_3 = col_3;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCol_1() {
        return col_1;
    }

    public void setCol_1(String col_1) {
        this.col_1 = col_1;
    }

    public int getCol_2() {
        return col_2;
    }

    public void setCol_2(int col_2) {
        this.col_2 = col_2;
    }

    public String getCol_3() {
        return col_3;
    }

    public void setCol_3(String col_3) {
        this.col_3 = col_3;
    }

    @Override
    public String toString() {
        return "com.buutcamp.springdemo.entities.BuutcampEntity{" +
                "id=" + id +
                ", col_1='" + col_1 + '\'' +
                ", col_2=" + col_2 +
                ", col_3='" + col_3 + '\'' +
                '}';
    }
}
